# 🌿 Enviro Plus Web - Installation and setup guide for Bookworm

## Install

To start with, open your terminal and let's update the package list and upgrade them to the latest available version:

```terminal
sudo apt update && sudo apt upgrade -y
```

Then, install the necessary dependencies in your Raspberry Pi:

```terminal
sudo apt install -y git python3-pip python3-cffi libportaudio2 python3-numpy python3-smbus python3-pil python3-setuptools python3-flask
```

After installation, enable i2c and SPI:

```terminal
sudo raspi-config nonint do_i2c 0
```

```terminal
sudo raspi-config nonint do_spi 0
```

Enable the serial login shell:

```terminal
sudo raspi-config nonint do_serial_cons 1
```

```terminal
sudo raspi-config nonint do_serial_hw 0
```

Also edit your config.txt file by typing:

```terminal
sudo nano /boot/firmware/config.txt
```

and add the following lines at the end of the file:

```terminal
dtoverlay=pi3-miniuart-bt
dtoverlay=adau7002-simple
```

Reboot your Raspberry Pi to apply these changes:

```terminal
sudo reboot now
```

Now it's time to install the Python libraries in the "enviroplusweb" virtual environment. For that, create a new one:

```terminal
python3 -m venv --system-site-packages $HOME/.virtualenvs/enviroplusweb
```

After creation, it has to be activated:

```terminal
source ~/.virtualenvs/enviroplusweb/bin/activate
```

And now the Enviro libraries can be installed:

```terminal
python3 -m pip install enviroplus
```

The system is ready to clone the project in your Raspberry Pi. To achieve this, type:

```terminal
git clone https://gitlab.com/idotj/enviroplusweb.git
```

All set, you can now run the app:

```terminal
cd enviroplusweb
```

```terminal
python enviroplusweb.py
```

Open your browser and type the IP address of your Raspberry Pi followed by port :8080, example: `http://192.168.1.142:8080`

## Setup

Open the file `config.py`, there you will find the following options to configure the application:

- Set your host ip address:

  ```python
  HOST = '0.0.0.0'
  ```

- Set your host port:

  ```python
  HOST_PORT = 8080
  ```

- Select the default language using the standard ISO 639 language code (if none is supported or set by the user):

  ```python
  LANGUAGE_DEFAULT = 'en'
  ```

- Keeps the Enviro LCD screen on or off:

  ```python
  LCD_SCREEN_ENABLED = True
  ```

- Enable/disable a fan plugged into your Raspberry Pi:

  ```python
  FAN_GPIO_ENABLED = True
  ```

- Set the GPIO pin for the fan:

  ```python
  FAN_GPIO_PIN = 4
  ```

- Displays values in the `metric` or `imperial` system. For example, the temperature is shown in degrees Celsius or degrees Farenheit:

  ```python
  SYSTEM_UNITS = 'metric'
  ```

- Compensates the temperature in relation to the cpu temperature (useful if you have the Enviro board directly connected to the RPi):

  ```python
  TEMP_CPU_COMPENSATION = True
  ```

- Modifies the multiplying factor on the values read by the temperature sensor:

  ```python
  TEMP_COMPENSATION_FACTOR = 3.10
  ```

- Modifies the multiplying factor on the values read by the humidity sensor:

  ```python
  HUMI_COMPENSATION_FACTOR = 1.50
  ```

- If you have an Enviro board with gas sensor, enable this option:

  ```python
  GAS_SENSOR = True
  ```

- If you have a particulate sensor [PMS5003](https://shop.pimoroni.com/products/pms5003-particulate-matter-sensor-with-cable?variant=29075640352851), enable this option:

  ```python
  PARTICULATE_SENSOR = True
  ```

- Set the latitude of your location (required to enable the Openweather feature):

  ```python
  LOCATION_LATITUDE = '50.850000'
  ```

- Set the longitude of your location (required to enable the Openweather feature):

  ```python
  LOCATION_LONGITUDE = '4.350000'
  ```

- Enables reading of external Openweather data such as wind direction and wind speed (you need an API key for this feature to work):

  ```python
  OPENWEATHER_ENABLED = True
  ```

- Set your generated API key from Openweathermap.org:

  ```python
  OPENWEATHER_API_KEY = ''
  ```

- Set Openweather API url:

  ```python
  OPENWEATHER_API_URL = 'https://api.openweathermap.org/data/2.5/weather'
  ```

- Frequency of call in seconds to Openweather to update readings. This value may vary depending on the type of your API account (free/paid).

  ```python
  OPENWEATHER_CALL_INTERVAL = 600
  ```

- If you need to debug and see more detail during the execution of the application:

  ```python
  DEBUG_LOGGING_ENABLED = True
  ```

### Extra setup

Maybe you want to run Enviro Plus Web at boot, then just type in the terminal:

```terminal
crontab -e
```

Add a new entry at the bottom with `@reboot` to specify that you want to run the command every time you restart your Raspberry Pi. Remember to replace in the path your HOSTNAME (if your default hostname is not 'pi').

```terminal
@reboot sleep 20 && /bin/bash -c 'cd /home/pi/enviroplusweb && source ~/.virtualenvs/enviroplusweb/bin/activate && sudo ~/.virtualenvs/enviroplusweb/bin/python /home/pi/enviroplusweb/enviroplusweb.py >> /home/pi/enviroplusweb/enviroplusweb.log 2>&1'
```
