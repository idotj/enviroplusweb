# -*- coding: utf-8 -*-

"""
Project: Enviro Plus Web
Description: Web interface for Enviro and Enviro+ sensor board plugged into a Raspberry Pi
Author: i.j
Version: 3.2.0
URL: https://gitlab.com/idotj/enviroplusweb
License: GNU
"""

from flask import Flask, render_template, request, redirect, abort
import RPi.GPIO as GPIO
import st7735
from fonts.ttf import RobotoMedium as UserFont
from PIL import Image, ImageDraw, ImageFont
from pms5003 import PMS5003
from enviroplus.noise import Noise
from enviroplus import gas
from bme280 import BME280
from smbus2 import SMBus
try:
    from ltr559 import LTR559
    ltr559 = LTR559()
except ImportError:
    import ltr559
import logging
import os
import glob
import threading
import json
import requests
from math import ceil, floor
from time import sleep, time, asctime, localtime, strftime
from config import Config

print("")
print("🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿")
print(" Initializing Enviro plus web v3.2.0 ")
print("🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿🌿")
print("")

# Language
logging.debug("Loading dictionary folder")
language_list = glob.glob("i18n/*.json")
languages = {}
# App init
logging.debug("Initializing app")
app = Flask(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = True
app_data = "enviroplusweb-data"
app_main_url = "/dashboard"
app_error_template = "error.html"
run_flag = True
# Init sensors
assert Config.GAS_SENSOR or not Config.PARTICULATE_SENSOR
bus = SMBus(1)
bme280 = BME280(i2c_dev=bus)
pms5003 = PMS5003()
noise = Noise()
cpu_temps = []
samples = 600
samples_per_day = 24 * 3600 // samples


# Load available languages
def init_languages():
    logging.debug(f"Loading {len(language_list)} dictionaries")
    for lang in language_list:
        lang_code = os.path.splitext(os.path.basename(lang))[0]

        with open(lang, "r", encoding="utf8") as file:
            languages[lang_code] = json.loads(file.read())


# Setup the fan
if Config.FAN_GPIO_ENABLED:
    logging.debug(f"Setting up GPIO fan on pin {Config.FAN_GPIO_PIN}")
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(Config.FAN_GPIO_PIN, GPIO.OUT)
    pwm = GPIO.PWM(Config.FAN_GPIO_PIN, 1000)
    pwm.start(100)


# Setup LCD display
if Config.LCD_SCREEN_ENABLED:
    logging.debug("Setting up LCD Screen")
    disp = st7735.ST7735(
        port=0,
        cs=1,
        dc="GPIO9",
        backlight="GPIO12",
        rotation=270,
        spi_speed_hz=10000000,
    )

    disp.begin()

    WIDTH = disp.width
    HEIGHT = disp.height

    color_above_threshold = (255, 0, 128)
    color_below_threshold = (64, 220, 220)
    color_within_threshold = (64, 220, 64)

    img = Image.new("RGB", (WIDTH, HEIGHT), color=(0, 0, 0))
    draw = ImageDraw.Draw(img)

    path = os.path.dirname(os.path.realpath(__file__)) + "/static/fonts"
    smallfont = ImageFont.truetype(path + "/asap/Asap-Bold.ttf", 10)
    x_offset = 2
    y_offset = 2
    unitTemp = "°C" if Config.SYSTEM_UNITS == "metric" else "°F"
    units = [unitTemp, "%", "hPa", "Lux", "u", "u", "u", "u"]

    if Config.GAS_SENSOR:
        units += ["kΩ", "kΩ", "kΩ"]

    if Config.PARTICULATE_SENSOR:
        units += ["μg/m3", "μg/m3", "μg/m3"]

    def display_everything():
        draw.rectangle((0, 0, WIDTH, HEIGHT), (0, 0, 0))
        column_count = 2
        variables = list(record.keys())
        row_count = ceil(len(units) / column_count)
        last_values = days[-1][-1]
        for i in range(len(units)):
            variable = variables[i + 1]
            data_value = record[variable]
            last_value = last_values[variable]
            unit = units[i]
            x = x_offset + (WIDTH // column_count) * (i // row_count)
            y = y_offset + (HEIGHT // row_count) * (i % row_count)
            message = "{}: {:s} {}".format(variable[:4], str(data_value), unit)
            tol = 1.01
            if data_value > last_value * tol:
                rgb = color_above_threshold
            elif data_value < last_value / tol:
                rgb = color_below_threshold
            else:
                rgb = color_within_threshold
            draw.text((x, y), message, font=smallfont, fill=rgb)
        disp.display(img)


# CPU temperature compensations to improve readings
def get_cpu_temperature():
    with open("/sys/class/thermal/thermal_zone0/temp", "r") as f:
        temp = f.read()
        temp = int(temp) / 1000.0
    return temp


if Config.TEMP_CPU_COMPENSATION:
    cpu_temps = [get_cpu_temperature()] * 5


# Get Temperature values
def get_temperature_readings():
    global cpu_temps
    raw_temp = bme280.get_temperature()
    if Config.TEMP_CPU_COMPENSATION:
        cpu_temp = get_cpu_temperature()
        cpu_temps = cpu_temps[1:] + [cpu_temp]
        avg_cpu_temp = sum(cpu_temps) / float(len(cpu_temps))
        temperature_scaled = raw_temp - (
            (avg_cpu_temp - raw_temp) / Config.TEMP_COMPENSATION_FACTOR
        )
        temperature = (
            temperature_scaled
            if Config.SYSTEM_UNITS == "metric"
            else temperature_scaled * 1.8 + 32
        )
    else:
        temp_compensated = raw_temp * Config.TEMP_COMPENSATION_FACTOR
        temperature = (
            temp_compensated
            if Config.SYSTEM_UNITS == "metric"
            else temp_compensated * 1.8 + 32
        )

    return {"temp": round(temperature, 1)}


# Get Humidity values
def get_humidity_readings():
    raw_humi = bme280.get_humidity()
    humidity = raw_humi * Config.HUMI_COMPENSATION_FACTOR
    return {"humi": round(humidity, 1)}


# Get Pressure values
def get_pressure_readings():
    pressure = bme280.get_pressure()
    return {"pres": round(pressure, 1)}


# Get Light values
def get_light_readings():
    lux = ltr559.get_lux()
    return {"lux": round(lux)}


# Fetch weather API data
def fetch_weather_data(url):
    try:
        response = requests.get(url, timeout=5)

        if response.status_code == 200:
            return response.json()
        else:
            logging.error(f"Error fetching weather API data: {response.status_code}")
            return {"error": f"Received error code {response.status_code}"}

    except requests.exceptions.Timeout:
        logging.error("Error: The request to weather API timed out.")
        return {"error": "The request timed out. Please try again later."}

    except requests.exceptions.ConnectionError:
        logging.error("Error: Unable to connect to weather API API.")
        return {
            "error": "Could not connect to the weather API. Check your network connection."
        }

    except requests.exceptions.RequestException as e:
        logging.error(f"Error: An unexpected error occurred: {e}")
        return {"error": f"An unexpected error occurred: {e}"}


# Get OpenWeather data
if Config.OPENWEATHER_ENABLED:
    openweather_url = f"{Config.OPENWEATHER_API_URL}?lat={Config.LOCATION_LATITUDE}&lon={Config.LOCATION_LONGITUDE}&appid={Config.OPENWEATHER_API_KEY}&units={Config.SYSTEM_UNITS}"
    openweather_data = []


def get_openweather_readings():
    global openweather_data

    if not hasattr(get_openweather_readings, "last_call_time"):
        get_openweather_readings.last_call_time = 0
        openweather_data = fetch_weather_data(openweather_url)
        return openweather_data

    if (
        time() - get_openweather_readings.last_call_time
        >= Config.OPENWEATHER_CALL_INTERVAL
    ):
        get_openweather_readings.last_call_time = time()
        openweather_data = fetch_weather_data(openweather_url)
        return openweather_data
    else:
        return openweather_data


# Get Wind values
def get_wind_readings():
    data = get_openweather_readings()
    if "error" in data:
        wind_direction = None
        wind_speed = None
    else:
        try:
            wind_direction = data["wind"]["deg"]
            wind_speed_raw = data["wind"]["speed"]
            if Config.SYSTEM_UNITS == "metric":
                wind_speed = round(wind_speed_raw * 3.6, 1)
            else:
                wind_speed = wind_speed_raw

        except KeyError:
            wind_direction = None
            wind_speed = None

    return {
        "windDir": wind_direction,
        "windSp": wind_speed,
    }


# Get Noise values
def get_noise_readings():
    low, mid, high, amp = noise.get_noise_profile()
    low *= 128
    mid *= 128
    high *= 128
    amp *= 64
    return {
        "high": round(high, 2),
        "mid": round(mid, 2),
        "low": round(low, 2),
        "amp": round(amp, 2),
    }


# Get Gas values
def get_gas_readings():
    gases = gas.read_all()
    oxi = round(gases.oxidising / 1000, 1)
    red = round(gases.reducing / 1000)
    nh3 = round(gases.nh3 / 1000)
    return {
        "oxi": oxi,
        "red": red,
        "nh3": nh3,
    }


# Get Particulate matter values
def get_particles_readings():
    while True:
        try:
            particles = pms5003.read()
            break
        except RuntimeError as e:
            logging.error("Particle read failed: %s", e.__class__.__name__)
            if not run_flag:
                raise e
            pms5003.reset()
            sleep(30)
    pm1 = particles.pm_ug_per_m3(1.0)
    pm25 = particles.pm_ug_per_m3(2.5)
    pm10 = particles.pm_ug_per_m3(10)

    return {
        "pm1": pm1,
        "pm25": pm25,
        "pm10": pm10,
    }


# Merge all readings
def get_readings_data(time):
    record = {"time": asctime(localtime(time))}
    record.update(get_temperature_readings())
    record.update(get_humidity_readings())
    record.update(get_pressure_readings())
    record.update(get_light_readings())

    if Config.OPENWEATHER_ENABLED:
        record.update(get_wind_readings())

    record.update(get_noise_readings())

    if Config.GAS_SENSOR:
        record.update(get_gas_readings())

    if Config.PARTICULATE_SENSOR:
        record.update(get_particles_readings())

    return record


record = get_readings_data(time())
data = []
days = []


# Generate filename with timestamp
def filename(t):
    return strftime(app_data + "/%Y_%j" + ".json", localtime(t))


# Get average values
def sum_data(data):
    totals = {"time": data[0]["time"]}
    keys = set().union(*(d.keys() for d in data))
    keys.discard("time")

    for key in keys:
        totals[key] = 0

    valid_counts = {key: 0 for key in keys}

    for d in data:
        for key in keys:
            value = d.get(key)
            if value is not None:
                totals[key] += value
                valid_counts[key] += 1

    for key in keys:
        if valid_counts[key] > 0:
            totals[key] = round(totals[key] / valid_counts[key], 1)
        else:
            totals[key] = None

    return totals



# Convert time in minutes since midnight
def record_time(r):
    t = r["time"].split()[3].split(":")
    return int(t[0]) * 60 + int(t[1])


# Save readings
def add_record(day, record):
    if record_time(record) > 0:
        while (
            len(day) == 0 or record_time(day[-1]) < record_time(record) - samples // 60
        ):
            if len(day):
                filler = dict(day[-1])
                t = record_time(filler) + samples // 60
            else:
                filler = dict(record)
                t = 0
            old_time = filler["time"]
            colon_pos = old_time.find(":")
            filler["time"] = (
                old_time[: colon_pos - 2]
                + ("%02d:%02d" % (t / 60, t % 60))
                + old_time[colon_pos + 3 :]
            )
            day.append(filler)
    day.append(record)


def compress_data(ndays, nsamples):
    cdata = []
    for day in days[-(ndays + 1) :]:
        for i in range(0, len(day), nsamples):
            cdata.append(sum_data(day[i : i + nsamples]))
    length = ndays * samples_per_day // nsamples
    return json.dumps(cdata[-length:])


# Load day readings
def read_day(fname):
    day = []
    with open(fname, "r") as f:
        for line in f.readlines():
            record = json.loads(line)
            add_record(day, record)
    return day


# Load readings files folder
def init_readings_data():
    if not os.path.isdir(app_data):
        os.makedirs(app_data)
    files = sorted(os.listdir(app_data))
    for f in files:
        days.append(read_day(app_data + "/" + f))


# Background tasks
def background():
    logging.debug("Initialize background tasks")
    global record, data, run_flag
    sleep(2)
    last_file = None
    while run_flag:
        t = int(floor(time()))
        record = get_readings_data(t)
        data = data[-(samples - 1) :] + [record]

        if t % samples == samples - 1 and len(data) == samples:
            totals = sum_data(data)
            fname = filename(t - (samples - 1))
            with open(fname, "a+") as f:
                f.write(json.dumps(totals) + "\n")
            if not days or (last_file and last_file != fname):
                days.append([])
            last_file = fname
            add_record(days[-1], totals)

        if Config.LCD_SCREEN_ENABLED and days:
            display_everything()

        sleep(max(t + 1 - time(), 0.1))


background_thread = threading.Thread(target=background)


def init_app():
    if Config.DEBUG_LOGGING_ENABLED:
        logging.basicConfig(
            level=logging.DEBUG,
            format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        )
        log = logging.getLogger("werkzeug")
        log.setLevel(logging.DEBUG)
    else:
        logging.disable(logging.CRITICAL)

    init_languages()
    init_readings_data()


def kill_app():
    logging.debug("Quitting the app")
    global run_flag
    run_flag = False
    GPIO.cleanup()
    if Config.LCD_SCREEN_ENABLED:
        disp.set_backlight(0)
    print("Waiting for background to quit...")
    background_thread.join()


@app.route("/")
@app.route(app_main_url, strict_slashes=False)
def redirect_dashboard():
    return redirect(f"{app_main_url}/{Config.LANGUAGE_DEFAULT}")


@app.route(f"{app_main_url}/<language>")
def main_page(language):
    if len(language) != 2:
        logging.error("Wrong url/path for language code")
        return abort(404)

    if not language.isalpha() or language not in languages:
        logging.error(
            "Language code in url/path not available, redirecting to default language"
        )
        return redirect(f"{app_main_url}/{Config.LANGUAGE_DEFAULT}")

    logging.debug("Rendering main page")
    return render_template(
        "index.html",
        **languages[language],
        gas_sensor=Config.GAS_SENSOR,
        particulate_sensor=Config.PARTICULATE_SENSOR,
        fan_gpio=Config.FAN_GPIO_ENABLED,
        system_units=Config.SYSTEM_UNITS,
        openweather=Config.OPENWEATHER_ENABLED,
    )


@app.route("/readings", strict_slashes=False)
def readings():
    if Config.FAN_GPIO_ENABLED:
        arg = request.args["fan"]
        pwm.ChangeDutyCycle(int(arg))
    return record


@app.route("/graph", strict_slashes=False)
def graph():
    arg = request.args["time"]
    if arg == "day":
        last2 = []
        for day in days[-2:]:
            last2 += day
        return json.dumps(last2[-samples_per_day:])
    if arg == "week":
        return compress_data(7, 30 * 60 // samples)
    if arg == "month":
        return compress_data(31, 120 * 60 // samples)
    if arg == "year":
        return compress_data(365, samples_per_day)
    return json.dumps(data)


@app.errorhandler(401)
def unauthorized(e):
    return render_template(app_error_template, error_message=e), 401


@app.errorhandler(403)
def forbidden(e):
    return render_template(app_error_template, error_message=e), 403


@app.errorhandler(404)
def page_not_found(e):
    return render_template(app_error_template, error_message=e), 404


@app.errorhandler(500)
def internal_server_error(e):
    return render_template(app_error_template, error_message=e), 500


if __name__ == "__main__":
    init_app()
    logging.debug("App initialized successfully")
    background_thread.start()
    logging.debug("Background thread started")
    try:
        app.run(
            debug=Config.DEBUG_LOGGING_ENABLED,
            host=Config.HOST,
            port=Config.HOST_PORT,
            use_reloader=False,
        )
    except Exception as e:
        logging.error("Failed to start the application", exc_info=True)
    finally:
        kill_app()
