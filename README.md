# 🌿 Enviro Plus Web

Web interface for [Enviro](https://shop.pimoroni.com/products/enviro?variant=31155658489939) and [Enviro+](https://shop.pimoroni.com/products/enviro?variant=31155658457171) sensor board plugged into a Raspberry Pi.  
This simple webapp serves a page with the current sensor readings in a graph over a specified time period.  
Runs thanks to [Flask](https://flask.palletsprojects.com) and [Chart.js](https://www.chartjs.org/)

![ScreenshotLightTheme](./docs/screenshot-lightTheme.jpg)

![ScreenshotDarkTheme](./docs/screenshot-darkTheme.jpg)

⚠️ Enviro readings must not be relied upon for critical applications.

## 📋 Table of contents

- [📖 User guide](#-user-guide)
  - [Raspberry Pi OS Bookworm](#raspberry-pi-os-bookworm)
  - [Raspberry Pi OS Bullseye (legacy)](#raspberry-pi-os-bullseye-legacy)
- [🚀 Improve me](#-improve-me)
- [⚖️ License](#️-license)
- [💬 FAQ](#-faq)
  - [Other answered questions](#other-answered-questions)

## 📖 User guide

Choose the tutorial that matches your Raspberry Pi OS:

### Raspberry Pi OS Bookworm

[Installation and setup for Bookworm](./docs/enviroplusweb-install-bookworm.md)

### Raspberry Pi OS Bullseye (legacy)

[Installation and setup for Bullseye](./docs/enviroplusweb-install-bullseye.md)

## 🚀 Improve me

Feel free to add your features and improvements.

## ⚖️ License

GNU Affero General Public License v3.0

## 💬 FAQ

- ### Where are my data readings saved?

  Your data will be stored in the same place where you have the application, in JSON format inside a folder called `/enviroplusweb-data`.

- ### How can I get my Raspberry Pi IP?

  Enter `hostname -I` in a Terminal window on your Raspberry Pi, then you will see the IPv4 and the IPv6.

- ### Graphs are empty, they don't draw any lines, but the live readings on the header are displayed

  You need to wait to have some data recorded in your Raspberry Pi. If you just run the app for first time, give it some time to save readings (~25min).

- ### The Enviro screen doesn't show any data

  If you just run the app for first time, it's normal. You must wait (~25min) until a file with the most recent readings is generated.

- ### I got an error related with 'adau7002' while running Enviroplusweb

  When I connect a display via HDMI to the Raspberry Pi Zero W, this error shows in the terminal:  
  `ValueError: No input device matching 'adau7002'`  
  Simply disconnecting the HDMI cable solves the problem.

  Another case where this error occurs, is after a reboot at application launch (no display connected).  
  It seems that the library that manages the audio is not very reliable. There is a [thread](https://github.com/pimoroni/enviroplus-python/issues/11) about this issue that is still open.  
  At the moment the quickest solution is to reboot your Raspberry Pi again.

- ### Raspberry Pi is running other services at localhost

  You can change the port to avoid any conflict with other applications. In that case edit the file `config.py` and find this line:

  ```python
  HOST_PORT = 8080
  ```

  Just change the `HOST_PORT` for another number (for example `4567`) and run again the app. Now you can access to your EnviroPlusWeb typing the ip address followed by `:4567`

- ### Enviro Plus Web is running but I can't connect to the web server through my browser

  If running your app, you can see in the terminal the following message:

  ```terminal
  * Serving Flask app 'enviroplusweb'
  * Debug mode: off
  Permission denied
  ```

  This problem may be due to `port=80` not being available to be used by the application and you have to use another port.

- ### I want to run my EnviroPlusWeb under HTTPS

  By default you use HTTP to connect to your Raspberry Pi through your browser, but some browsers will redirect automatically to HTTPS. If you prefer to have your project running under HTTPS here you have a tutorial explaning how to setup Flask with HTTPS:  
  <https://blog.miguelgrinberg.com/post/running-your-flask-application-over-https>

- ### Sometimes my Raspberry Pi disconnects from the wifi and I can't connect again

  There is an option that manages the power of the wifi and allows to put it in saving mode. Disabling this option may help you to avoid this problem. First check if the wifi power save feature is enabled or not:

  ```terminal
  iw wlan0 get power_save
  ```

  If it is enabled, then edit the following file. Remember to replace in the path your HOSTNAME (if your default hostname is not 'pi'):

  ```terminal
  sudo nano /home/pi/.bashrc
  ```

  And add the following line at the end:

  ```terminal
  sudo iwconfig wlan0 power off
  ```

  Reboot and check again typing the first command to see if the feature is enabled or not.

### Other answered questions

Check the [closed issues](https://gitlab.com/idotj/enviroplusweb/-/issues/?sort=created_date&state=closed&first_page_size=20), you might find your question there.  
If nothing matches with your problem, check the [open issues](https://gitlab.com/idotj/enviroplusweb/-/issues/?sort=created_date&state=opened&first_page_size=20) or feel free to create a new one.
